#include <Arduino.h>
#include <RGBLed.h>
#include <Rotary.h>
#include <EEPROM.h>
#include "Key.h"

#define SWITCH_PIN 3
#define MEM_POS_COLOR_COUNT 0
#define MEM_POS_COLORS_START 1

const Color RED = Color(255,0,0);
const Color GREEN = Color(0,255,0);
const Color BLUE = Color(0,0,255);
const Color WHITE = Color(255,255,255);

enum Direction{
    UP, DOWN
};
void adjust_led(Direction dir);
void adjust_led(uint8_t count, Direction dir);
void handle_programming(unsigned int);
void handle_switch_running(unsigned int);
void save_color(Color color, uint8_t pos);
Color get_color(uint8_t pos);


auto led = RGBLed(CATHODE, 5, 6, 11);
// auto led = RGBLed(CATHODE, 7, 11, 12);
Rotary r = Rotary(9, 10);
uint8_t red = 255;
uint8_t green = 0;
uint8_t blue = 0;


Key rotary_switch = Key(SWITCH_PIN);

#define RUNNING 1
#define PROGRAMMING 2
uint8_t state;

uint8_t currentColorMemPos;

uint8_t colorCount;

void setup() {
    state = RUNNING;
    colorCount = EEPROM.get(MEM_POS_COLOR_COUNT, colorCount);
    pinMode(5, INPUT);

    // GIMSK |= (1 << PCIE) | (1<<INT1); //Enable Pin Change Interrupts
    // PCMSK |= (1 << PCINT0) | (1 << PCINT1) | (1<<PCINT4); // Enable Interrupts for PINB0 and PINB1
    EIMSK |= (1<<INT1);

    TCCR1A = (1<<COM1A1) | (1<<COM1A0) | (1<<WGM10);// Change pwm behaviour to avoid flicker
    TCCR2B = (1<<CS21);
    // MCUCR |= (1<<ISC10);

    PCICR |= (1<<PCIE0);
    PCMSK0 |= (1 << PCINT2) | (1 << PCINT1); // Enable Interrupts for PINB0 and PINB1
    EICRA |= (1<<ISC10);
    led.setup();
    rotary_switch.setup();
    led.on(get_color(0));
    sei();
}

void loop() {
    rotary_switch.process();
    if(!rotary_switch.isPressed()){
        unsigned int press_time = rotary_switch.getPressTime();
        switch(state){
            case PROGRAMMING:
                handle_programming(press_time);
                break;
            case RUNNING:
                handle_switch_running(press_time);
                break;
        }
    }
}

void handle_switch_running(unsigned int press_time){
    if(press_time > 100){
        state = PROGRAMMING;
        colorCount = 0;
        led.blink(BLUE, 10, 3);
        led.on(Color(red, green, blue));
    }else if(press_time > 0){

        currentColorMemPos++;
        if(currentColorMemPos >= colorCount){
            currentColorMemPos = 255;// Will overflow on next iteration
            led.off();
            return;
        }
        led.on(get_color(currentColorMemPos));
    }
}

void handle_programming(unsigned int press_time){
    if(press_time > 100){
        state = RUNNING;
        EEPROM.put(MEM_POS_COLOR_COUNT, colorCount);
        currentColorMemPos = 0;
        led.blink(GREEN, 10, 3);
        led.on(get_color(currentColorMemPos));
    }else if(press_time > 0){
        Color col = Color(red, green, blue);
        save_color(col, colorCount);
        led.blink(col, 10, 2);
        led.on(col);
        colorCount++;
    }
}

void save_color(Color color, uint8_t pos){
    EEPROM.put(pos * sizeof(Color) + MEM_POS_COLORS_START, color);
}

Color get_color(uint8_t pos){
    Color color = Color(0,0,0);
    EEPROM.get(pos * sizeof(Color) + MEM_POS_COLORS_START, color);
    return color;
}

// void adjust_led_v2(uint8_t count, Direction dir){
//     if(blue > 0 && blue < 255){
//         blue += dir == UP ? count : -count;
//     } else if(red > 0 && red < 255){
//         red += dir == UP ? count : - count;
//     } else if(green > 0 && green < 255){
//         green += dir == UP ? count : -count;
//     } else {
//         if(dir == UP){
//             if(blue == 255 && red == 255){
//                 blue -= count;
//             }else if(red == 255 && green == 255){
//                 red -= count;
//             }
//         }
//     }
// }

void adjust_led(uint8_t count, Direction dir){
    if(dir == UP){
        if(blue == 255 && red != 255){
            if(green == 0){
                red += count;
            }else if(red == 0){
                green-= count;
            }
        }else if(red == 255 && green != 255){
            if(blue == 0){
                green += count;
            }else if(green == 0){
                blue -=count;
            }
        } else if(green == 255 && blue != 255){
            if(red == 0){
                blue +=count;
            } else if(blue == 0){
                red -=count;
           }

        }
    }else{
        if(blue == 255 && green != 255){
            if(red == 0){
                green += count;
            }else if(green == 0){
                red -= count;
           }
        } else if(green == 255 && red != 255){
            if(blue == 0){
                red += count;
            } else if(red == 0){
                blue -= count;
            }
        } else if(red == 255 && blue != 255){
            if(green == 0){
                blue += count;
            } else if(blue == 0){
                green -= count;
           }
        }
    }
    led.on(Color(red, green, blue));
}
void adjust_led(Direction dir){
    adjust_led(85, dir);
}

void adjust_brightness(Direction dir){
    
}

// unsigned long last_press;
ISR(INT1_vect){
    // adjust_led(UP);
    // rotary_switch.process();
}

//Interrupt fired by rotary encoder
ISR(PCINT0_vect) {
// ISR(PCINT_vect) {
    unsigned char result = r.process();
    switch (state){
        case RUNNING:
            if (result == DIR_CCW) {
            //   digitalWrite(pos, HIGH);
                adjust_brightness(DOWN);
            } else if (result == DIR_CW) {
            //   digitalWrite(pos, HIGH);
                adjust_brightness(UP);
            }
            break;
        case PROGRAMMING:
            if (result == DIR_CCW) {
            //   digitalWrite(pos, HIGH);
                adjust_led(DOWN);
            } else if (result == DIR_CW) {
            //   digitalWrite(pos, HIGH);
                adjust_led(UP);
            }
            break;
    }
}

