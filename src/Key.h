#ifndef KEY_H
#define KEY_H
#include <Arduino.h>

class Key{
    public:
        Key(uint8_t pin):pin(pin){}

        unsigned int getPressTime();
        uint8_t isPressed();
        void process();
        void setup();

    private:
        uint8_t pin;
        volatile unsigned int pressTime;
        unsigned int lastProcess;
        uint8_t is_pressed;
        uint8_t wasPressed;
        uint8_t clickCount;
};
#endif