#include "Key.h"

unsigned int Key::getPressTime(){
    unsigned int pressTime_v = pressTime;
    if(!isPressed()){
        pressTime = 0;
    }
    return pressTime_v;
}

uint8_t Key::isPressed(){
    return is_pressed;
}

void Key::process(){
    is_pressed = digitalRead(pin);
    unsigned int now = millis();
    if(is_pressed && !wasPressed){
        wasPressed = true;
    }else if(is_pressed && wasPressed){
        pressTime += (now - lastProcess);
    }else if(!is_pressed && wasPressed) {
        pressTime += (now - lastProcess);
        wasPressed = false;
    }else{
        wasPressed = false;
    }
    lastProcess = now;
}

void Key::setup(){
    pinMode(pin, INPUT);
}